package com.example.datingapp.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * n
 * Created by Mobile Developer on 2/22/2017.e
 */

public class HttpApiService {

    public static String BASE_URL = "https://randomuser.me";

    private static HttpApiEndPoint mHttpApiService;

    static {
        setupClient();
    }

    public static HttpApiEndPoint getHttpApiEndPoint() {
        if (mHttpApiService != null) {
            mHttpApiService = null;
        }
        setupClient();
        return mHttpApiService;
    }

    public static void setupClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .hostnameVerifier((s, sslSession) -> true);
        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL + "/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
        mHttpApiService = retrofit.create(HttpApiEndPoint.class);
    }
}
