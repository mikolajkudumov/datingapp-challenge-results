package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

public class DateData {

    @SerializedName("date")
    public String date;

    @SerializedName("age")
    public int age;

}
