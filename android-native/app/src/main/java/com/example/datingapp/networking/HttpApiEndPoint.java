package com.example.datingapp.networking;

import com.example.datingapp.model.ProfileResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Mobile Developer on 2/22/2017.
 */

public interface HttpApiEndPoint {

    /*
       Check app update
    */
    @GET("api")
    Call<ProfileResult> getProfiles();
}
