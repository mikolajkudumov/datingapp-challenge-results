package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

public class Coordinate {

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

}
