package com.example.datingapp.ui.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.datingapp.R;
import com.example.datingapp.model.Profile;
import com.example.datingapp.model.ProfileResult;
import com.example.datingapp.networking.HttpApiService;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<ProfileResult> {

    public ActionBar actionBar;
    KProgressHUD hud;
    int count = 1;
    TextView txt_count;

    @BindView(R.id.img_avatar)
    CircleImageView img_avatar;

    @BindView(R.id.txt_name)
    TextView txt_name;

    @BindView(R.id.txt_age)
    TextView txt_age;

    @BindView(R.id.ly_yes)
    ViewGroup ly_yes;

    @BindView(R.id.ly_no)
    ViewGroup ly_no;

    @BindView(R.id.txt_yes)
    TextView txt_yes;

    @BindView(R.id.txt_no)
    TextView txt_no;

    ViewGroup bg_count;

    boolean is_yes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initViews();
    }

    void initViews() {
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.dark_gray)));
            actionBar.setHomeButtonEnabled(false);
            actionBar.setElevation(0);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            LayoutInflater inflator = LayoutInflater.from(this);
            View v = inflator.inflate(R.layout.actionbar_title, null);
            v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            txt_count = v.findViewById(R.id.txt_count);
            bg_count = v.findViewById(R.id.bg_count);

            actionBar.setCustomView(v);
        }
        getProfiles();
    }

    @SuppressLint("DefaultLocale")
    void setProfile(Profile profile) {
        if (is_yes) {
            count++;
        }
        if (count == 5) {
            ly_no.setEnabled(false);
            ly_yes.setEnabled(false);
            bg_count.setBackgroundResource(R.drawable.bg_rectangle_orange);
            ly_yes.setBackgroundColor(ContextCompat.getColor(this, R.color.dark_gray));
            txt_yes.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
            txt_no.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
        }
        txt_count.setText(String.format("%d", count));
        txt_name.setText(String.format("%s %s", profile.name.first, profile.name.last));
        txt_age.setText(String.format("(%d)", profile.dob.age));
        Picasso.get().load(profile.picture.large).into(img_avatar);
    }

    void getProfiles() {
        showHUD();
        HttpApiService.getHttpApiEndPoint().getProfiles().enqueue(this);
    }

    @Override
    public void onResponse(Call<ProfileResult> call, Response<ProfileResult> response) {
        hideHUD();
        ProfileResult result = response.body();
        if (response.isSuccessful() && result != null) {
            if (result.info.results > 0) {
                setProfile(result.results.get(0));
            }
        } else {
            // handle error
        }
    }

    @OnClick({R.id.ly_no, R.id.ly_yes})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.ly_no:
                is_yes = false;
                break;
            case R.id.ly_yes:
                is_yes = true;
                break;
        }
        getProfiles();
    }

    @Override
    public void onFailure(Call<ProfileResult> call, Throwable t) {
        hideHUD();
    }

    public void showHUD() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Loading...")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void hideHUD() {
        hud.dismiss();
    }
}