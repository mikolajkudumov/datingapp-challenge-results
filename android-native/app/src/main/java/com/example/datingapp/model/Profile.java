package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("gender")
    public String gender;

    @SerializedName("name")
    public Name name;

    @SerializedName("location")
    public Location location;

    @SerializedName("email")
    public String email;

    @SerializedName("login")
    public Login login;

    @SerializedName("dob")
    public DateData dob;

    @SerializedName("registered")
    public DateData registered;

    @SerializedName("phone")
    public String phone;

    @SerializedName("cell")
    public String cell;

    @SerializedName("id")
    public ID id;

    @SerializedName("picture")
    public Picture picture;

    @SerializedName("nat")
    public String nat;
}
