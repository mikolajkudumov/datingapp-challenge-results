package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

public class ID {

    @SerializedName("name")
    public String name;

    @SerializedName("value")
    public String value;

}
