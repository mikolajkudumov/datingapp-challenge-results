package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

public class Location {

//    @SerializedName("street")
//    public String street;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("postcode")
    public String postcode;

    @SerializedName("coordinate")
    public Coordinate coordinate;

    @SerializedName("timezone")
    public Timezone timezone;
}
