package com.example.datingapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProfileResult {

    @SerializedName("results")
    public List<Profile> results = new ArrayList<>();

    @SerializedName("info")
    public Info info;

}
