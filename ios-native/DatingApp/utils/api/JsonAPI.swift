//
//  JsonAPI.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import Moya

let JsonAPIProvider = MoyaProvider<JsonAPI>()

enum JsonAPI {
    case getUserProfile
}

extension JsonAPI : TargetType {
    
    var baseURL: URL {
        
        guard let url = URL(string: BASE_URL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        
        switch self {
        case .getUserProfile:
            return ""
        }
    }
    var method: Moya.Method {
        switch self {
        case .getUserProfile:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        switch self {
        case .getUserProfile:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return [
            "Content-type": "application/json",
        ]
    }
}


extension Moya.Response {
    func jsonAPIResponse() throws -> Dictionary<String, Any> {
        let any = try self.mapJSON()
        guard let dic = any as? Dictionary<String, Any> else {
            throw MoyaError.jsonMapping(self)
        }
        return dic
    }
}

