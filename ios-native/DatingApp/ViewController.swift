//
//  ViewController.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import UIKit
import JGProgressHUD

class ViewController: SuperViewController {
    
    var profileResult = ProfileResult()
    var count = 1
    @IBOutlet weak var imgAvatar: CircleImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var bgCounter: RoundedView!
    @IBOutlet weak var lblCounter: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initViews()
    }

    func initViews() {        
        
        fetchProfile(yes: false)
    }
    
    func fetchProfile(yes: Bool) {
        self.showHUD()
        JsonAPIProvider.request(JsonAPI.getUserProfile, completion: { (result) in
            self.hideHUD()
            switch result {
            case .success(_):
                do {
                    let resp = try result.get()
                    self.profileResult = try resp.map(ProfileResult.self)
                    self.updateViews(yes: yes)
                } catch let err { // should handle error
                    print(err)
                }
            case let .failure(error): // should handle error
                print(error)
            }
        })
    }
    
    func updateViews(yes: Bool) {
        if yes {
            count = count + 1
        }
        lblCounter.text = "\(count)"
        if count == 5 {
            btnNo.isEnabled = false
            btnYes.isEnabled = false
            bgCounter.backgroundColor = UIColor.init(named: "Orange")
            btnYes.backgroundColor = UIColor.init(named: "DarkGray")
            btnYes.setTitleColor(UIColor.init(named: "LightGray"), for: .normal)
            btnNo.setTitleColor(UIColor.init(named: "LightGray"), for: .normal)
        }
        if self.profileResult.info.results > 0 {
            let profile = profileResult.results[0]
            lblName.text = "\(profile.name.first) \(profile.name.last)"
            lblAge.text = String.init(format: "(%d)", profile.dob.age)
            imgAvatar.load(url: URL.init(string: profile.picture.large)!)
        }
    }

    @IBAction func onYes(_ sender: Any) {
        fetchProfile(yes: true)
    }
    
    @IBAction func onNo(_ sender: Any) {
        fetchProfile(yes: false)
    }
    
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
