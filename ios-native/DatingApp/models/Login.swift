//
//  Login.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Login {
    var uuid = ""
    var username = ""
    var password = ""
    var salt = ""
    var md5 = ""
    var sha1 = ""
    var sha256 = ""
}

extension Login: Decodable {
    
    enum LoginCodingKeys: String, CodingKey {
        case uuid
        case username
        case password
        case salt
        case md5
        case sha1
        case sha256
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: LoginCodingKeys.self)
        do {
            uuid = try container.decode(String.self, forKey: .uuid)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            username = try container.decode(String.self, forKey: .username)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            password = try container.decode(String.self, forKey: .password)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            salt = try container.decode(String.self, forKey: .salt)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            md5 = try container.decode(String.self, forKey: .md5)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            sha1 = try container.decode(String.self, forKey: .sha1)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            sha256 = try container.decode(String.self, forKey: .sha256)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
