//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Info {
    var seed = ""
    var results = 0
    var page = 0
    var version = ""
}

extension Info: Decodable {
    
    enum InfoCodingKeys: String, CodingKey {
        case seed
        case results
        case page
        case version
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: InfoCodingKeys.self)
        do {
            seed = try container.decode(String.self, forKey: .seed)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            results = try container.decode(Int.self, forKey: .results)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            page = try container.decode(Int.self, forKey: .page)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            version = try container.decode(String.self, forKey: .version)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
