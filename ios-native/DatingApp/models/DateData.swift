//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct DateData {
    var date = ""
    var age = 0
}

extension DateData: Decodable {
    
    enum DateDataCodingKeys: String, CodingKey {
        case date
        case age
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DateDataCodingKeys.self)
        do {
            date = try container.decode(String.self, forKey: .date)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            age = try container.decode(Int.self, forKey: .age)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
