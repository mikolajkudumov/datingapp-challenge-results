//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Picture {
    var large = ""
    var medium = ""
    var thumbnail = ""
}

extension Picture: Decodable {
    
    enum PictureCodingKeys: String, CodingKey {
        case large
        case medium
        case thumbnail
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PictureCodingKeys.self)
        do {
            large = try container.decode(String.self, forKey: .large)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            medium = try container.decode(String.self, forKey: .medium)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            thumbnail = try container.decode(String.self, forKey: .thumbnail)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
