//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Location {
    var street = ""
    var city = ""
    var state = ""
    var postcode = ""
    var coordinate = Coordinate()
    var timezone = Timezone()
}

extension Location: Decodable {
    
    enum LocationCodingKeys: String, CodingKey {
        case street
        case city
        case state
        case postcode
        case coordinates
        case timezone
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: LocationCodingKeys.self)
        do {
            street = try container.decode(String.self, forKey: .street)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            city = try container.decode(String.self, forKey: .city)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            state = try container.decode(String.self, forKey: .state)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            postcode = try container.decode(String.self, forKey: .postcode)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            coordinate = try container.decode(Coordinate.self, forKey: .coordinates)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            timezone = try container.decode(Timezone.self, forKey: .timezone)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
