//
//  ProfileResult.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct ProfileResult {
    var results = [Profile]()
    var info = Info()
}

extension ProfileResult: Decodable {
    
    enum ProfileResultCodingKeys: String, CodingKey {
        case results
        case info
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ProfileResultCodingKeys.self)
        do {
            results = try container.decode([Profile].self, forKey: .results)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            info = try container.decode(Info.self, forKey: .info)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
