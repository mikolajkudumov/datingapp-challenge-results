//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Timezone {
    var offset = ""
    var description = ""
}

extension Timezone: Decodable {
    
    enum TimezoneCodingKeys: String, CodingKey {
        case offset
        case description
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TimezoneCodingKeys.self)
        do {
            offset = try container.decode(String.self, forKey: .offset)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            description = try container.decode(String.self, forKey: .description)
        } catch let error as NSError {
            print(error.localizedDescription)
        }        
    }
}
