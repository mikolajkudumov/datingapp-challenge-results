//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Name {
    var title = ""
    var first = ""
    var last = ""
}

extension Name: Decodable {
    
    enum NameCodingKeys: String, CodingKey {
        case title
        case first
        case last
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NameCodingKeys.self)
        do {
            title = try container.decode(String.self, forKey: .title)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            first = try container.decode(String.self, forKey: .first)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            last = try container.decode(String.self, forKey: .last)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
