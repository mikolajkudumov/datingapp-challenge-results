//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Coordinate {
    var latitude = ""
    var longitude = ""
}

extension Coordinate: Decodable {
    
    enum CoordinateCodingKeys: String, CodingKey {
        case latitude
        case longitude
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CoordinateCodingKeys.self)
        do {
            latitude = try container.decode(String.self, forKey: .latitude)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            longitude = try container.decode(String.self, forKey: .longitude)
        } catch let error as NSError {
            print(error.localizedDescription)
        }        
    }
}
