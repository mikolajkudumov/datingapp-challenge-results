//
//  ProfileResult.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct Profile {
    var gender = ""
    var name = Name()
    var location = Location()
    var email = ""
    var login = Login()
    var dob = DateData()
    var registered = DateData()
    var phone = ""
    var cell = ""
    var id = ID()
    var picture = Picture()
    var nat = ""
}

extension Profile: Decodable {
    
    enum ProfileCodingKeys: String, CodingKey {
        case gender
        case name
        case location
        case email
        case login
        case dob
        case registered
        case phone
        case cell
        case id
        case picture
        case nat
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ProfileCodingKeys.self)
        do {
            gender = try container.decode(String.self, forKey: .gender)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            name = try container.decode(Name.self, forKey: .name)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            location = try container.decode(Location.self, forKey: .location)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            email = try container.decode(String.self, forKey: .email)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            login = try container.decode(Login.self, forKey: .login)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            dob = try container.decode(DateData.self, forKey: .dob)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            registered = try container.decode(DateData.self, forKey: .registered)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            phone = try container.decode(String.self, forKey: .phone)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            cell = try container.decode(String.self, forKey: .cell)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            id = try container.decode(ID.self, forKey: .id)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            picture = try container.decode(Picture.self, forKey: .picture)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            nat = try container.decode(String.self, forKey: .nat)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
