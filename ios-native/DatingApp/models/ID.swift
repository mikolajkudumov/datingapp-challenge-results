//
//  Name.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import Foundation
import ObjectMapper

struct ID {
    var name = ""
    var value = ""
}

extension ID: Decodable {
    
    enum IDCodingKeys: String, CodingKey {
        case name
        case value
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: IDCodingKeys.self)
        do {
            name = try container.decode(String.self, forKey: .name)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        do {
            value = try container.decode(String.self, forKey: .value)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
