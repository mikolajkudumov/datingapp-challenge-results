//
//  RoundedView.swift
//
//  Created by Dev & Ops on 2021/8/19.
//

import UIKit

class CircleImageView: UIImageView {
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        
    }
    
}
