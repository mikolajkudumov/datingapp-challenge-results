//
//  SuperViewController.swift
//  DatingApp
//
//  Created by Dev & Ops on 2021/8/18.
//

import UIKit
import JGProgressHUD

class SuperViewController: UIViewController {
    let hud = JGProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hud.textLabel.text = "Loading..."
    }
    
    func showHUD() {
        hud.show(in: self.view)
                    
    }
    
    func hideHUD() {
        hud.dismiss()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
