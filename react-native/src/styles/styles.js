import { StyleSheet } from "react-native";
import palette from './palette';

export default (styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
        width: '100%',        
        display: 'flex',
        flexDirection: 'column',
        justifyContent:'space-between'
    },
    headerContainer: {
        backgroundColor: palette.DarkGrey,
        height: 60,
        width: '100%',        
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerTitle: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 16,
        fontFamily: 'Roboto'
    },
    headerRightTitle: {
        color: '#000',
        backgroundColor: '#fff',
        fontSize: 14,
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 4,
        marginRight: 16
    },
    bodyContainer: {
        backgroundColor: '#fff',        
        width: '100%',        
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    profileContainer: {                
        width: '100%',        
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    profileAvatar: {
        width: 128,
        height: 128,
        borderRadius: 100        
    },
    profileName: {
        color: '#000',
        fontSize: 32,
        fontFamily: 'Roboto'
    },
    profileAge: {
        color: palette.LightGrey,
        fontSize: 14,
        color: '#000',
        fontFamily: 'Roboto'
    },
    buttonContainer: {                
        width: '100%',        
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',             
    },
    noButton: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: palette.DarkGrey,
        height: 60,        
    },
    yesButton: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: palette.Orange,
        height: 60,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
        fontFamily: 'Roboto'
    }
}));
