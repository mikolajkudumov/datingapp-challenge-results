import React, {useState, useEffect} from 'react';
import {TouchableOpacity, SafeAreaView, Text, View, Image} from 'react-native';
import axios from 'axios';
import Constants from '../utils/constants';
import palette from '../styles/palette';
import styles from '../styles/styles';

function ProfileScreen() {
  const [profileData, setProfileData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [counter, setCounter] = useState(1);

  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8;',
    },
  };
  /* Search Endpoint : /api/autocomplete/search */

  const fetchData = async isIncreaseCounter => {
    try {
      setIsLoading(true);
      if (isIncreaseCounter) {
        setCounter(counter + 1);
      }
      const response = await axios.get(Constants.API_URL, config);
      if (response.status === 200) {
        setProfileData(response.data.results[0]);
      }
    } catch (error) {
      console.log('error=', error);
      return error;
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const isDisabled = counter === 5 || isLoading;

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>Gender Neutral Dating App</Text>
        <Text style={[
              styles.headerRightTitle,
              {backgroundColor: counter === 5 ? palette.Orange : 'white'},
            ]}>{counter}</Text>
      </View>
      <View style={styles.bodyContainer}>
        {!isLoading && profileData ? (
          <View style={styles.profileContainer}>
            <Image
              source={{uri: profileData.picture.large}}
              style={styles.profileAvatar}
            />
            <Text style={styles.profileName}>{`${profileData.name.first} ${
              profileData.name.last
            }`}</Text>
            <Text style={styles.profileAge}>({profileData.dob.age})</Text>
          </View>
        ) : (
          <Text>Loading...</Text>
        )}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          disabled={isDisabled}
          style={styles.noButton}
          onPress={() => fetchData(false)}>
          <Text
            style={[
              styles.buttonText,
              {color: isDisabled ? palette.LightGrey : 'white'},
            ]}>
            No
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={isDisabled}
          style={[
            styles.yesButton,
            {backgroundColor: isDisabled ? palette.DarkGrey : palette.Orange},
          ]}
          onPress={() => fetchData(true)}>
          <Text
            style={[
              styles.buttonText,
              {color: isDisabled ? palette.LightGrey : 'white'},
            ]}>
            Yes
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

export default ProfileScreen;
